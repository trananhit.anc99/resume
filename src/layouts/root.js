import React from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { Switch, Route } from "react-router-dom";
import DashboardRoutes from 'router';
import {Container} from 'react-bootstrap';
import {
    Router
  } from "react-router-dom";

  import './styleLayout.sass';

const SwitchRoute = (
    <Switch>
        {DashboardRoutes.map((route, i) => <Route path={route.path} component={route.component} key={i} ></Route>)}
    </Switch>
);
const Root = ({store, hist}) => (
    <Provider store={store}>
        <Router history={hist}>
            <Container className="mobileUi">
                {SwitchRoute}
            </Container>
        </Router>
    </Provider>
)

Root.propTypes = {
    store: PropTypes.object.isRequired
}

export default Root