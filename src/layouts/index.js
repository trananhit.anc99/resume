import React from 'react'
import {Container} from 'react-bootstrap';
import { Switch, Route } from "react-router-dom";
// Import route
import DashboardRoutes from 'router'
// Import Sass
import './styleLayout.sass';


const SwitchRoute = (
    <Switch>
        {DashboardRoutes.map((route, i) => <Route path={route.path} component={route.component} key={i} ></Route>)}
    </Switch>
);
const Main = () => {

    return (
        <Container className="mobileUi">
            {SwitchRoute}
        </Container>
        
    )
}

export default Main;