import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import configureStore from 'store'
// Import css and SASS
import 'assets/sass/main.sass';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { createBrowserHistory } from "history";
import Root from 'layouts/root';
const store = configureStore();
const hist = createBrowserHistory();
ReactDOM.render(
  <React.StrictMode>
    {/* <Router history={hist}>
      <Switch>
        <Route path={hist.location.pathname} component={Main}></Route>
      </Switch>
    </Router> */}
    <Root store={store} hist={hist}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
