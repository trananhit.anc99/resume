import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import { getData } from 'functions/getData'
import './App.css';
import {
  Link
} from "react-router-dom";
const API = '';
function App() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const data = getData(API);
    data
    .then((result) => {
      console.log(result);
    })
    .catch((err) => {
      console.log(err);
    })
  }, [])
  console.log(data);
  return (
    <div className="App">
      <header className="App-header">
        <Link to='/home'>Home</Link>
      </header>
      <ul>
        {data.map((el, i) => <li key={i}>{el.author}</li>)}
      </ul>
    </div>
  );
}

export default App;
