import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import Trending from "components/trending/trending";
import {Row} from 'react-bootstrap';
import { getData } from 'functions/getData';
import { sliceString } from 'functions/sliceString';
import './styleSlide.sass';
const API = 'http://newsapi.org/v2/everything?q=apple&from=2020-05-12&to=2020-05-12&sortBy=popularity&apiKey=a365185ffebd4d7d8a2c66262ba3ee95';
const SlideBlog = () => {
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1
    };
    const [data, setData] = useState([{}])
    useEffect(() => {
    const data = getData(API);
    data
        .then((result) => {
            const { articles } = result.data;
            setData(articles);
        })
        .catch((err) => {
            console.log(err)
        })
    }, []);
    
    return (
        <Row>
            <div className="col-md-1 customTitle">
                <div className="title_category">
                    <p>JS</p>
                </div>
            </div>
            <div className="col-md-11 customBlog">
                <Slider {...settings}>
                    {data.map((el, i) => 
                    el.urlToImage && el.title !== null
                        ?
                        <div className='customSlideBlog' key={i}>
                            <Trending imgUrl={el.urlToImage} title={sliceString(el.title)}></Trending>
                        </div>
                        : ''
                    )}
                </Slider>
            </div>
        </Row>
    );
}

export default SlideBlog;