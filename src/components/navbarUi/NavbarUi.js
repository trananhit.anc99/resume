import React, {useState} from 'react';
import { Link } from 'react-router-dom';
// Import sass
import './navbarUi.sass'
const NavbarUI = ({temp, location, icon}) => {
    const [toggle, setToggle] = useState(false);
    const handleToggle = () => {
        setToggle(!toggle);
        console.log(toggle);
    }
    return(
        <div className='navbarUi'>
            <div className="logo">
                <h2>Tran Anh</h2>
            </div>
            <div className={ toggle ? 'activeNavbarPath navbarPath' : 'navbarPath'}>
                <ul>
                    <li className='searchMobile'>search</li>
                    <li>
                        <Link exact to='/home'>Home</Link>
                    </li>
                    <li>
                        <Link to='/blog'>Blog</Link>
                    </li>
                    <li>
                        <Link exact to='/'>About Me</Link>
                    </li>
                </ul>
            </div>
            <div className="search">

            </div>
            <div className="weather">
                <img src={icon} alt="icon-temp"/>
                <div className="location-temp">
                    <p className="temperature">{temp}<sup>o</sup>C</p>
                    <p className="location">{location}</p>
                </div>
            </div>

            <div onClick={handleToggle} className="bugger">
                <div className="lineOne"></div>
                <div className="lineTwo"></div>
                <div className="lineThree"></div>
            </div>
        </div>
    )
}

export default NavbarUI;