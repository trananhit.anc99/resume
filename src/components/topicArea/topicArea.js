import React from 'react';

import './topicArea.sass'
const TopicArea = () => {
    return(
        <div className="topicArea">
            <div className="img_topic">
                <img src="https://miro.medium.com/fit/c/60/60/1*AQbRi7322aPUWTzp_zOhTg.png" alt="alts"/>
            </div>
            <p>Javascript</p>
        </div>
    );
}
export default TopicArea;