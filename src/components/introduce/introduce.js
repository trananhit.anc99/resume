import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import './introduce.sass';
const Introduce = () => {
    return(
        <Container>
        <Row>
            <Col sm='12' md='8' className='order-1'>
                <div className="customCol">
                    <div className="banner_img">
                        <img src="https://res.cloudinary.com/samanthaming/image/upload/f_auto,q_auto/v1587966908/tidbits/87-5-ways-to-append-item-to-array.jpg" alt="alt"/>
                    </div>
                    <div className="content_text">
                        <h2 className='title__post'>Hi! I am Development</h2>
                        <p className='content__post'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua....</p>
                        <div className="time__post_read">
                            <span className='timePost'>Apr 17 </span>
                            <span className='timeRead'>12 min read</span>
                        </div>
                        <div className="read__more">
                            <p>Read more</p>
                        </div>
                    </div>
                </div>
            </Col>
            <Col sm='12' md='4' className='order-2'>
                <div className="introduceMySelf">
                    <h2>Hi! I ‘m a Font-End Development</h2>
                    <p>I'm a Front End Developer who shares programming goodness with the
                    community</p>
                </div>
            </Col>
        </Row>
    </Container>
    );
}
export default Introduce;