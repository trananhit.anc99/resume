import React from 'react'
import './trendingBlog.sass';
const TrendingBlog = ({imgUrl, title}) => {
    return (
        <div className="blog_new">
            <div className="post__img">
                <img src={imgUrl} alt="post_img"/>
                <div className="title__post_new">
                <p>{title}</p>
                </div>
            </div>
        </div>
    );
}

export default TrendingBlog;