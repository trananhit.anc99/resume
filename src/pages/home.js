import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {Row, Col} from 'react-bootstrap';

// {/* Import components core */}
import Navbar from 'components/navbarUi/NavbarUi';
import Introduce from 'components/introduce/introduce';
import Trending from 'components/trending/trending';
import SlideBlog from 'components/slideBlog/slideBlog';
// import TopicPost from 'components/topicArea/topicArea';
// {/* Import actions */}
import { fetchApiWeather } from 'actions';
// {/* Import function core */}
import { sliceString, convertKtoC, isPlainObject} from 'functions/sliceString';
// {/* End Import function core */}
import Icon_Temp from 'assets/img/SVG/Asset.svg';
import './home.sass';
// Import Apis
import { ApiWeather } from 'storeApis';
const HomePages = () => {
    const [data, setData] = useState([]);
    const dispatch = useDispatch()
    const {dataWeather} = useSelector(state => state.dataWeathers);
    useEffect(() => {
        dispatch(fetchApiWeather());
    }, []);
    return(
        <div className="homePage">
            {
                isPlainObject(dataWeather) ? <Navbar temp={convertKtoC(dataWeather.main.temp)} location={dataWeather.name} icon={Icon_Temp}></Navbar> : ''
            }
            <Introduce></Introduce>
            <div className="areaTrending">
                <div className="title_trending">
                    <h2>New Blog</h2>
                </div>
                <Row>
                    {data.map((el, i) => 
                        el.urlToImage !== null ?
                        <Col md='3' key={i}>
                            <Trending imgUrl={el.urlToImage} title={sliceString(el.title)}></Trending>
                        </Col>
                        : ''
                    )}
                </Row>
            </div>
            <div className="areaCategoryBlog">
                <SlideBlog></SlideBlog>
            </div>
        </div>
    );  
}

export default HomePages;