// Import component core
import HomePage from 'pages/home'
import App from 'App';
const dashboardRoutes = [
/**
 * @param {string} path
 * @param {string} name
 * @param {function} component
*/
    {
        path: "/home",
        name: "home",
        component:HomePage,
    },
    {
        path: "/",
        name: "home",
        component:App,
    }
]

export default dashboardRoutes;