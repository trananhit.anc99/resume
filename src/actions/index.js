import * as type from "constants/index.js";
import axios from 'axios';
//Api
import { ApiWeather } from 'storeApis';


export const fetchApiWeatherRequest = () => {
  return {
    type: type.FETCH_WEATHER_REQUEST
  };
}
const fetchApiWeatherFailure = (err) => {
  return {
    type: type.FETCH_WEATHER_FAILURE,
    payload: err
  };
}
export const fetchApiWeatherSuccess = (data) => {
    return {
      type: type.FETCH_WEATHER_SUCCESS,
      payload: data
    }
  };
export const fetchApiWeather = (Apis= ApiWeather) => {
  return (dispatch) => {
    dispatch(fetchApiWeatherRequest)
    return axios.get(Apis)
      .then(response => {
        dispatch(fetchApiWeatherSuccess(response.data))
      })
      .catch(error => {
        const errMsg = error.message
        dispatch(fetchApiWeatherFailure(errMsg));
      });
  };
};
// FETCH_WEATHER_REQUEST = 'FETCH_WEATHER_REQUEST';
// FETCH_WEATHER_SUCCESS = 'FETCH_WEATHER_SUCCESS';
// FETCH_WEATHER_FAILURE = 'FETCH_WEATHER_FAILURE';