export const sliceString = ((string, start=0, end=30) => `${string.slice(start, end)}...`);
export const convertKtoC = ((numK) => numK - 273.15);
export const isPlainObject = v => (!!v && typeof v === 'object' && (v.__proto__ === null || v.__proto__ === Object.prototype));

