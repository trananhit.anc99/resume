import React, {useState} from 'react'
import { getData } from 'functions/getData';
import axios from 'axios';
export const useGetData = (Apis) => {
    const [data, setData] = useState([]);
    const resultData =  getData(Apis);
    resultData
        .then((result) => {
            const dataApis = result.data;
            setData(dataApis)
        })
        .catch((err) => {
            throw err;
        })
    return data;
}
export const usePostData = (apiPost, payload) => {
    const resultPost = axios.post(apiPost, payload)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    return resultPost;
}