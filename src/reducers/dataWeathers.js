const initialState = {
    loading: false,
    dataWeathers: [],
    error: ''
};
const dataWeathers = (state=initialState, action) => {

    switch (action.type) {
        case "FETCH_WEATHER_REQUEST":
            return {
                ...state,
                loading: true
            };
        case "FETCH_WEATHER_SUCCESS":
            return {
                loading: false,
                dataWeather: action.payload,
                error: ''
            };
        case "FETCH_WEATHER_FAILURE":
            return {
                loading: false,
                dataWeather: [],
                error: action.payload
            };
        default:
            return state;
    }
}

export default dataWeathers;