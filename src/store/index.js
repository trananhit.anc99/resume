import thunk from 'redux-thunk';
import logger from 'redux-logger';
import myReducers from "reducers";
import { createStore, applyMiddleware, compose } from "redux";
const middleware = [thunk, logger];
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;
const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
);

export default function configureStore() {
    return createStore(
        myReducers,
        enhancer
    )
}